package com.ihsan.footballapps.adapter

import android.content.Context
import android.content.Intent
import android.provider.CalendarContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ihsan.footballapps.R
import com.ihsan.footballapps.model.Event
import com.ihsan.footballapps.model.Team
import com.ihsan.footballapps.utils.getDateCalendar
import com.ihsan.footballapps.utils.getDateFormat
import com.ihsan.footballapps.utils.getTimeFormat
import com.ihsan.footballapps.views.event.detailevent.DetailEventActivity
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_list.view.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.startActivity

class EventsAdapter(private val context: Context, private val items: List<Event>)
    : RecyclerView.Adapter<EventsAdapter.ViewHolder>(), AnkoLogger {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventsAdapter.ViewHolder {

        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.activity_list, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: EventsAdapter.ViewHolder, position: Int) {

        holder.bindItem(items[position])
    }

    inner class ViewHolder(private val containerView: View) : RecyclerView.ViewHolder(containerView), EventView {


        private val fatchEvent = MainPresenter(this, AndroidSchedulers.mainThread())

        fun bindItem(items: Event) {

            fatchEvent.requestTeam(items.idHomeTeam.toString(), true)
            fatchEvent.requestTeam(items.idAwayTeam.toString(), false)

            val strDate = items.dateEvent
            var strTime = items.strTime

            if (strTime?.length == 8) {
                strTime = items.strTime + "+00:00"
            } else if (strTime?.length == 5) {
                strTime = items.strTime + ":00+00:00"
            }


//            info(items.dateEvent+"T"+items.strTime+" = "+getDateCalendar(strTime, strDate) +"format = " +getDateFormat(strTime, strDate))

            containerView.clubHome.text = items.strHomeTeam.toString()
            containerView.clubAway.text = items.strAwayTeam.toString()
            containerView.dateEvent.text = getDateFormat(strTime, strDate)
            containerView.timeEvents.text = getTimeFormat(strTime, strDate)
            containerView.scoreAway.text = items.scoreAway
            containerView.scoreHome.text = items.scoreHome

            var isNext = true

            if (items.scoreAway.toString() != "null") {
                isNext = false
                containerView.addCalendar.visibility = View.INVISIBLE
            }

            containerView.addCalendar.setOnClickListener {

                val intent = Intent(Intent.ACTION_EDIT)
                intent.type = "vnd.android.cursor.item/event"
                intent.putExtra(CalendarContract.Events.TITLE, items.strEvent)
                intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, getDateCalendar(strTime, strDate)?.time)
                intent.putExtra(CalendarContract.Events.ALL_DAY, false)
                context.startActivity(intent)
            }
            containerView.setOnClickListener {
                context.startActivity<DetailEventActivity>("idEvent" to items.eventId.toString(),
                        "idHome" to items.idHomeTeam.toString(),
                        "idAway" to items.idAwayTeam.toString(),
                        "isNext" to isNext)

            }

        }

        override fun showTeam(data: List<Team>, boolean: Boolean) {

            if (boolean) {
                Picasso.get().load(data[0].teamBadge).into(itemView.imgHome)
            } else {
                Picasso.get().load(data[0].teamBadge).into(itemView.imgAway)
            }
        }


    }


}