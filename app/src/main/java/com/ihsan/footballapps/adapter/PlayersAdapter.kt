package com.ihsan.footballapps.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ihsan.footballapps.R
import com.ihsan.footballapps.model.Player
import com.ihsan.footballapps.views.team.playerdetail.PlayerDetailActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_list_players.view.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.startActivity


class PlayersAdapter(private val context: Context, private val items: MutableList<Player>)
    : RecyclerView.Adapter<PlayersAdapter.ViewHolder>(), AnkoLogger {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayersAdapter.ViewHolder {

        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.activity_list_players, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: PlayersAdapter.ViewHolder, position: Int) {

        holder.bindItem(items[position])
    }

    inner class ViewHolder(private val containerView: View) : RecyclerView.ViewHolder(containerView) {

        fun bindItem(items: Player) {

            if (items.strCutout == null) {
                Picasso.get().load(R.drawable.profile).into(itemView.imgPlayer)
            } else {
                Picasso.get().load(items.strCutout).into(itemView.imgPlayer)
            }

            containerView.playerName.text = items.strPlayer.toString()
            containerView.playerPosition.text = items.strPosition.toString()

            containerView.setOnClickListener {

                context.startActivity<PlayerDetailActivity>("idPlayer" to items.idPlayer)

            }

        }


    }


}