package com.ihsan.footballapps.adapter

import com.ihsan.footballapps.api.ApiClient
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import io.reactivex.Scheduler


class MainPresenter(val view: EventView, private val mainSchedulers: Scheduler) : AnkoLogger {

    private val apiClient by lazy {
        ApiClient.create()
    }

    private var disposable: Disposable? = null


    fun requestTeam(idTeam: String, boolean: Boolean) {

        disposable = apiClient?.getTeam(idTeam)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(mainSchedulers)
                ?.subscribe({
                    view.showTeam(it.teams, boolean)
                }, {
                    info(it.message.toString())

                })
    }


}