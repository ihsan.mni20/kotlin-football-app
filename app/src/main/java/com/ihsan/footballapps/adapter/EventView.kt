package com.ihsan.footballapps.adapter

import com.ihsan.footballapps.model.Team

interface EventView {
    fun showTeam (data:List<Team>, boolean: Boolean)
}