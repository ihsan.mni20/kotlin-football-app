package com.ihsan.footballapps.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ihsan.footballapps.R
import com.ihsan.footballapps.model.Team
import com.ihsan.footballapps.views.team.detailteam.DetailTeamsActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_list_teams.view.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.startActivity


class TeamsAdapter(private val context: Context, private val items: MutableList<Team>)
    : RecyclerView.Adapter<TeamsAdapter.ViewHolder>(), AnkoLogger {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamsAdapter.ViewHolder {

        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.activity_list_teams, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: TeamsAdapter.ViewHolder, position: Int) {

        holder.bindItem(items[position])
    }

    inner class ViewHolder(private val containerView: View) : RecyclerView.ViewHolder(containerView){

        fun bindItem(items: Team) {

            Picasso.get().load(items.teamBadge).into(itemView.imgTeam)

            containerView.teamName.text = items.teamName.toString()

            containerView.setOnClickListener {

                context.startActivity<DetailTeamsActivity>("idTeam" to items.teamId.toString(),
                        "teamDes" to items.teamDescribe.toString(), "strTeam" to items.teamName.toString())

            }

        }



    }


}