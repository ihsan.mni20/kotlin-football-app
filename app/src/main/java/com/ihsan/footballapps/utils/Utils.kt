package com.ihsan.footballapps.utils

import android.annotation.SuppressLint
import android.content.Context
import com.ihsan.footballapps.db.FavDBOpenHelper
import java.text.SimpleDateFormat
import java.util.*

val Context.favDatabase: FavDBOpenHelper
    get() = FavDBOpenHelper.getInstance(applicationContext)


@SuppressLint("SimpleDateFormat")

fun getDateFormat(time: String?, date: String?): String? {

    val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz")
    val inDate = formatter.parse(date + "T" + time)
    val dateEvent = SimpleDateFormat("EEEE, dd MMM yyyy")

    return dateEvent.format(inDate)
}

@SuppressLint("SimpleDateFormat")
fun getTimeFormat(time: String?, date: String?): String? {

    val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz")
    val inDate = formatter.parse(date + "T" + time)
    val timeEvent = SimpleDateFormat("HH:mm")

    return timeEvent.format(inDate)
}

@SuppressLint("SimpleDateFormat")
fun getDateCalendar(time: String?, date: String?): Date? {

    val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz")

    return formatter.parse(date + "T" + time)
}
