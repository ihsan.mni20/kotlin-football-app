package com.ihsan.footballapps.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.ihsan.footballapps.model.Event
import com.ihsan.footballapps.model.Team
import org.jetbrains.anko.db.*

class FavDBOpenHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "FootballApps.db", null, 1) {

    companion object {
        private var instance: FavDBOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): FavDBOpenHelper {
            if (instance == null) {
                instance = FavDBOpenHelper(ctx.applicationContext)
            }

            return instance as FavDBOpenHelper
        }
    }


    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(Event.TABLE_FAVORITE_EVENT, true,
                Event.EVENT_ID to TEXT + PRIMARY_KEY,
                Event.DATE_EVENT to TEXT,
                Event.STR_EVENT to TEXT,

                Event.ID_HOME_TEAM to TEXT,
                Event.STR_HOME_TEAM to TEXT,
                Event.SCORE_HOME to TEXT,
                Event.STR_HOME_LINEUP_GOALKEEPER to TEXT,
                Event.STR_HOME_LINEUP_DEFENSE to TEXT,
                Event.STR_HOME_LINEUP_MIDFIELD to TEXT,
                Event.STR_HOME_LINEUP_FORWARD to TEXT,
                Event.STR_HOME_LINEUP_SUBSTITUTES to TEXT,
                Event.STR_HOME_GOAL_DETAILS to TEXT,

                Event.STR_AWAY_GOAL_DETAILS to TEXT,
                Event.ID_AWAY_TEAM to TEXT,
                Event.STR_AWAY_TEAM to TEXT,
                Event.SCORE_AWAY to TEXT,
                Event.STR_AWAY_LINEUP_GOALKEEPER to TEXT,
                Event.STR_AWAY_LINEUP_DEFENSE to TEXT,
                Event.STR_AWAY_LINEUP_MIDFIELD to TEXT,
                Event.STR_AWAY_LINEUP_FORWARD to TEXT,
                Event.STR_AWAY_LINEUP_SUBSTITUTES to TEXT,
                Event.STR_TIME to TEXT)

        db?.createTable(Team.TABLE_FAVORITE_TEAM, true,
                Team.TEAM_ID to TEXT + PRIMARY_KEY,
                Team.TEAM_NAME to TEXT,
                Team.TEAM_YEAR to TEXT,
                Team.TEAM_STADIUM to TEXT,
                Team.TEAM_DESCRIPTION to TEXT,
                Team.TEAM_BADGE to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable(Event.TABLE_FAVORITE_EVENT, true)
        db.dropTable(Team.TABLE_FAVORITE_TEAM, true)
    }


}


