package com.ihsan.footballapps.api

import com.ihsan.footballapps.BuildConfig
import com.ihsan.footballapps.model.EventResponse
import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit


interface ApiClient {

    @GET("eventsnextleague.php")
    fun getEventNext(@Query("id") idLeague: String): Observable<EventResponse>

    @GET("eventspastleague.php")
    fun getEventPast(@Query("id") idLeague: String): Observable<EventResponse>

    @GET("searchevents.php")
    fun getSearchEvent(@Query("e") strTeam: String): Observable<EventResponse>

    @GET("search_all_leagues.php")
    fun getLeague(@Query("s") strSport: String): Observable<EventResponse>

    @GET("lookupteam.php")
    fun getTeam(@Query("id") idTeam: String): Observable<EventResponse>

    @GET("lookupevent.php")
    fun getEvent(@Query("id") idEvent: String): Observable<EventResponse>

    @GET("search_all_teams.php")
    fun getAllTeams(@Query("l") strLeague: String): Observable<EventResponse>

    @GET("searchteams.php")
    fun getSearchTeams(@Query("t") strTeam: String): Observable<EventResponse>

    @GET("searchplayers.php")
    fun getPlayersTeams(@Query("t") strTeam: String): Observable<EventResponse>

    @GET("lookupplayer.php")
    fun getPlayersDetail(@Query("id") idPlayer: String): Observable<EventResponse>

    companion object {
        fun create(): ApiClient? {

            val okHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build()

            val baseUr = BuildConfig.BASE_URL + "api/v1/json/${BuildConfig.API_KEY}" + "/"
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .baseUrl(baseUr)
                    .build()

            return retrofit.create(ApiClient::class.java)
        }
    }
}