package com.ihsan.footballapps.views.event.nextevent

import com.ihsan.footballapps.model.Event
import com.ihsan.footballapps.model.League

interface NextEventView{
    fun showEventList (data:List<Event>)
    fun showLeague (data:List<League>)
}