package com.ihsan.footballapps.views.team.detailteam


import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.ihsan.footballapps.R
import com.ihsan.footballapps.model.Team
import com.ihsan.footballapps.utils.favDatabase
import com.ihsan.footballapps.views.team.overviewteam.OverviewTeamsFragment
import com.ihsan.footballapps.views.team.playerteam.PlayerTeamFragment
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_detail_teams.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class DetailTeamsActivity : AppCompatActivity(), DetailTeamsView, AnkoLogger {

    private lateinit var detailTeamsPresenter: DetailTeamsPresenter

    private var menuItem: Menu? = null

    private var isFavorites: Boolean = false

    private lateinit var toolbar: ActionBar

    private lateinit var teamId: String

    private lateinit var teams: List<Team>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_teams)


        toolbar = supportActionBar!!
        toolbar.elevation = 0F

        teamId = intent.getStringExtra("idTeam").toString()

        val teamDes = intent.getStringExtra("teamDes")
        val strTeam = intent.getStringExtra("strTeam")

        detailTeamsPresenter = DetailTeamsPresenter(this, AndroidSchedulers.mainThread())
        detailTeamsPresenter.requestTeam(teamId)

        val overviewTeamsFragment = OverviewTeamsFragment()
        val playerTeamFragment = PlayerTeamFragment()

        val bundle = Bundle()
        bundle.putString("ovr", teamDes)
        bundle.putString("strTeam", strTeam)

        overviewTeamsFragment.arguments = bundle
        playerTeamFragment.arguments = bundle
        val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        viewPagerAdapter.addFragment(overviewTeamsFragment, "Overview")
        viewPagerAdapter.addFragment(playerTeamFragment, "players")

        vpTeam.adapter = viewPagerAdapter
        tab_team.setupWithViewPager(vpTeam)
        favoriteState()


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.detail_menu, menu)
        menuItem = menu
        setFavorites()

        return super.onCreateOptionsMenu(menu)
    }

    private fun favoriteState() {
        favDatabase.use {
            val result = select(Team.TABLE_FAVORITE_TEAM)
                    .whereArgs("TEAM_ID={id}",
                            "id" to teamId)
            val favorite = result.parseList(classParser<Team>())

            if (favorite.isNotEmpty()) isFavorites = true

        }
    }

    private fun addFavorite() {
        try {
            favDatabase.use {
                insert(Team.TABLE_FAVORITE_TEAM,
                        Team.TEAM_ID to teams[0].teamId,
                        Team.TEAM_NAME to teams[0].teamName,
                        Team.TEAM_YEAR to teams[0].teamYear,
                        Team.TEAM_STADIUM to teams[0].teamStadium,
                        Team.TEAM_DESCRIPTION to teams[0].teamDescribe,
                        Team.TEAM_BADGE to teams[0].teamBadge)
            }
        } catch (e: SQLiteConstraintException) {

        }
    }

    private fun favoriteRemove() {
        favDatabase.use {
            delete(Team.TABLE_FAVORITE_TEAM, "TEAM_ID={id}",
                    "id" to teamId)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {

            android.R.id.home -> {
                finish()
                true
            }
            R.id.addToFavorites -> {
                if (isFavorites) favoriteRemove() else addFavorite()
                isFavorites = !isFavorites
                setFavorites()

                true
            }


            else -> super.onOptionsItemSelected(item)

        }
    }

    private fun setFavorites() {
        if (isFavorites) {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorites)
        } else {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_add_to_favorites)
        }

    }

    override fun showTeam(data: List<Team>) {
        Picasso.get().load(data[0].teamBadge).into(ivTeam)
        tvNameTeam.text = data[0].teamName
        tvYear.text = data[0].teamYear
        tvStadium.text = data[0].teamStadium

        teams = data

    }

    internal class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {

        private val mFragmentList = arrayListOf<Fragment>()
        private val mFragmentTitle = arrayListOf<String>()


        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitle.add(title)
        }


        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitle[position]
        }


    }

}
