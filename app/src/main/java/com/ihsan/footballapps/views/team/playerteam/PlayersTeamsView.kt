package com.ihsan.footballapps.views.team.playerteam

import com.ihsan.footballapps.model.Player

interface PlayersTeamsView {
    fun showPlayerTeam(data: List<Player>)
}