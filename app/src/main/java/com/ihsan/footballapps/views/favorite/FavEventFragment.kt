package com.ihsan.footballapps.views.favorite


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ihsan.footballapps.R
import com.ihsan.footballapps.adapter.EventsAdapter
import com.ihsan.footballapps.model.Event
import com.ihsan.footballapps.utils.favDatabase
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.support.v4.ctx


class FavEventFragment : Fragment() {

    private var events: MutableList<Event> = mutableListOf()
    private lateinit var adapter: EventsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_fav_event, container, false)
        val rvList = view.findViewById(R.id.rvListFavEvent) as RecyclerView
        adapter = EventsAdapter(ctx, events)
        rvList.adapter = adapter
        rvList.layoutManager = LinearLayoutManager(ctx)

        events.clear()

        showFavEventList()

        return view

    }

    private fun showFavEventList() {
        context?.favDatabase?.use {

            val result = select(Event.TABLE_FAVORITE_EVENT)
            val favorite = result.parseList(classParser<Event>())
            events.addAll(favorite)
            adapter.notifyDataSetChanged()

        }

    }

    override fun onResume() {
        events.clear()
        showFavEventList()
        super.onResume()
    }


}
