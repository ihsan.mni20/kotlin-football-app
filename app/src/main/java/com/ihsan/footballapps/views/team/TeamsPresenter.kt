package com.ihsan.footballapps.views.team

import com.ihsan.footballapps.api.ApiClient
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import io.reactivex.Scheduler

class TeamsPresenter(val view: TeamsView, private val mainSchedulers: Scheduler) : AnkoLogger {

    private val apiClient by lazy {
        ApiClient.create()
    }

    private var disposable: Disposable? = null

    fun requestLeague() {
        disposable = apiClient?.getLeague("Soccer")
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(mainSchedulers)
                ?.subscribe({
                    view.showLeague(it.countrys)
                }, {
                    info(it.message.toString())

                })
    }

    fun requestTeamList(strLeague: String) {

        disposable = apiClient?.getAllTeams(strLeague)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(mainSchedulers)
                ?.subscribe({
                    view.showTeamList(it.teams)
                }, {
                    view.showTeamList(emptyList())
                    info(it.message.toString())

                })
    }


}