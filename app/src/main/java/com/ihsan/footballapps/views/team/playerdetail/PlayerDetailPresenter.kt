package com.ihsan.footballapps.views.team.playerdetail

import com.ihsan.footballapps.api.ApiClient
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class PlayerDetailPresenter(val view: PlayerDetailView, private val mainSchedulers: Scheduler) : AnkoLogger {

    private val apiClient by lazy {
        ApiClient.create()
    }

    private var disposable: Disposable? = null


    fun requestTeamDetail(idPlayer: String) {

        disposable = apiClient?.getPlayersDetail(idPlayer)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(mainSchedulers)
                ?.subscribe({
                    view.showTeamDetail(it.players)
                }, {
                    view.showTeamDetail(emptyList())
                    info(it.message.toString())

                })
    }


}