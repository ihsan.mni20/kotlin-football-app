package com.ihsan.footballapps.views.event.detailevent

import com.ihsan.footballapps.model.Event
import com.ihsan.footballapps.model.Team

interface DetailEventView {
    fun showTeam (data:List<Team>, boolean: Boolean)
    fun showEvent (data:List<Event>, boolean: Boolean)
}