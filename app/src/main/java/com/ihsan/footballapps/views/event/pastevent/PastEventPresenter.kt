package com.ihsan.footballapps.views.event.pastevent

import com.ihsan.footballapps.api.ApiClient
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class PastEventPresenter(val view: PastEventView, private val mainSchedulers: Scheduler) : AnkoLogger {

    private val apiClient by lazy {
        ApiClient.create()
    }

    private var disposable: Disposable? = null

    fun requestLeague() {
        disposable = apiClient?.getLeague("Soccer")
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(mainSchedulers)
                ?.subscribe({
                    view.showLeague(it.countrys)
                }, {
                    info(it.message.toString())

                })
    }

    fun requestPastEvent(idLeague: String) {

        disposable = apiClient?.getEventPast(idLeague)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(mainSchedulers)
                ?.subscribe({
                    view.showEventList(it.events)

                }, {
                    view.showEventList(emptyList())
                    info(it.message.toString())

                })
    }
}