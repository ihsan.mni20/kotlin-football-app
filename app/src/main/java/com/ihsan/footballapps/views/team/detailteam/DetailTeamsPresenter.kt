package com.ihsan.footballapps.views.team.detailteam

import com.ihsan.footballapps.api.ApiClient
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info


class DetailTeamsPresenter(val view: DetailTeamsView, private val mainSchedulers: Scheduler) : AnkoLogger {

    private val apiClient by lazy {
        ApiClient.create()
    }

    private var disposable: Disposable? = null

    fun requestTeam(idTeam: String) {

        disposable = apiClient?.getTeam(idTeam)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(mainSchedulers)
                ?.subscribe({
                    view.showTeam(it.teams)
                }, {
                    view.showTeam(emptyList())
                    info(it.message.toString())

                })
    }




}