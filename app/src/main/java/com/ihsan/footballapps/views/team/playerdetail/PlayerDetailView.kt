package com.ihsan.footballapps.views.team.playerdetail

import com.ihsan.footballapps.model.Player

interface PlayerDetailView{
    fun showTeamDetail (data: List<Player>)
}