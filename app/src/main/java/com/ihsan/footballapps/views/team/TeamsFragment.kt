package com.ihsan.footballapps.views.team


import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ihsan.footballapps.R
import com.ihsan.footballapps.adapter.TeamsAdapter
import com.ihsan.footballapps.model.League
import com.ihsan.footballapps.model.Team
import com.ihsan.footballapps.views.search.SearchActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_teams.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.ctx

class TeamsFragment : Fragment(), TeamsView, SearchView.OnQueryTextListener {
    override fun onQueryTextSubmit(query: String?): Boolean {
        ctx.startActivity<SearchActivity>("searchTxt" to query, "isTeam" to true)
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return true
    }

    override fun showTeamList(data: List<Team>) {
        teams.clear()
        teams.addAll(data)
        adapter.notifyDataSetChanged()

        rvListTeam.visibility = View.VISIBLE
        tvNoDataTeam.visibility = View.INVISIBLE
        pgTeam.visibility = View.INVISIBLE

        if (teams.isEmpty()) {

            tvNoDataTeam.visibility = View.VISIBLE
            rvListTeam.visibility = View.INVISIBLE
            teams.clear()

        }
    }

    private var teams: MutableList<Team> = mutableListOf()
    private lateinit var adapter: TeamsAdapter
    private lateinit var teamsPresenter: TeamsPresenter
    private var nameLeague: ArrayList<String> = arrayListOf()
    private var idLeague: ArrayList<String> = arrayListOf()
    private lateinit var mContext: Context
    private lateinit var searchView: SearchView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_teams, container, false)
        val rvList = view.findViewById(R.id.rvListTeam) as RecyclerView
        rvList.layoutManager = LinearLayoutManager(view.context)

        adapter = TeamsAdapter(view.context, teams)

        rvList.adapter = adapter

        this.teamsPresenter = TeamsPresenter(this, AndroidSchedulers.mainThread())
        //teamsPresenter.requestTeamList("English Premier League")
        teamsPresenter.requestLeague()


        return view

    }

    companion object {
        fun newInstance(): TeamsFragment = TeamsFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        mContext = this.activity!!
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        val mSearch = menu.findItem(R.id.idSearch)
        searchView = mSearch?.actionView as SearchView

        searchView.setOnQueryTextListener(this)
        searchView.queryHint = getString(R.string.hint_search)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun showLeague(data: List<League>) {

        for (i in data.indices) {
            nameLeague.add(data[i].strLeague.toString())
            idLeague.add(data[i].leagueId.toString())
        }

        val spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, nameLeague)

        idSpinnerTeam.adapter = spinnerAdapter
        idSpinnerTeam.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, spinnerItems: Long) {

                //context?.toast(nameLeague[position])
                teamsPresenter.requestTeamList(nameLeague[position])
            }
        }
    }
}
