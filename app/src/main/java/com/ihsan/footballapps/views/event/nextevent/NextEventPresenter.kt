package com.ihsan.footballapps.views.event.nextevent

import com.ihsan.footballapps.api.ApiClient
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class NextEventPresenter(val view: NextEventView, private val mainSchedulers: Scheduler) : AnkoLogger {

    private val apiClient by lazy {
        ApiClient.create()
    }

    private var disposable: Disposable? = null

    fun requestLeague() {
        disposable = apiClient?.getLeague("Soccer")
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(mainSchedulers)
                ?.subscribe({
                    view.showLeague(it.countrys)

                }, {
                    info(it.message.toString())

                })
    }

    fun requestNextEvent(idLeague: String) {
        disposable = apiClient?.getEventNext(idLeague)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(mainSchedulers)
                ?.subscribe({

                    view.showEventList(it.events)
                }, {
                    view.showEventList(emptyList())
                    info(it.message.toString())

                })
    }
}