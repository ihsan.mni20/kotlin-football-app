package com.ihsan.footballapps.views.team.playerdetail

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.ihsan.footballapps.R
import com.ihsan.footballapps.model.Player
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_player_detail.*

class PlayerDetailActivity : AppCompatActivity(), PlayerDetailView {
    override fun showTeamDetail(data: List<Player>) {

        heightPlayer.text = data[0].strHeight
        weightPlayer.text = data[0].strWeight
        positionPlayers.text = data[0].strPosition
        overPlayers.text = data[0].strDescriptionEN
        toolbar.title = data[0].strPlayer
        if (data[0].strFanart2 == null) {
            Picasso.get().load(R.drawable.ballfield).into(imgPlayers)
        } else {
            Picasso.get().load(data[0].strFanart2).into(imgPlayers)
        }

    }

    private lateinit var presenter: PlayerDetailPresenter
    private lateinit var toolbar: ActionBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player_detail)
        toolbar = supportActionBar!!
        toolbar.setDisplayShowHomeEnabled(true)
        toolbar.setDisplayHomeAsUpEnabled(true)

        val idPlayers = intent.getStringExtra("idPlayer")

        presenter = PlayerDetailPresenter(this, AndroidSchedulers.mainThread())
        idPlayers?.let { presenter.requestTeamDetail(it) }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {

            android.R.id.home -> {
                finish()
                true
            }

            else -> super.onOptionsItemSelected(item)

        }
    }
}
