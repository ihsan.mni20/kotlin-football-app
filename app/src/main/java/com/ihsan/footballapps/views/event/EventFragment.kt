package com.ihsan.footballapps.views.event

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.ihsan.footballapps.R
import com.ihsan.footballapps.views.event.nextevent.NextFragment
import com.ihsan.footballapps.views.event.pastevent.PastEventFragment
import com.ihsan.footballapps.views.search.SearchActivity
import kotlinx.android.synthetic.main.fragment_event.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.ctx

class EventFragment : Fragment(), SearchView.OnQueryTextListener {
    override fun onQueryTextSubmit(query: String?): Boolean {


        ctx.startActivity<SearchActivity>("searchTxt" to query, "isTeam" to false)
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return true
    }

    private lateinit var mContext: Context
    private lateinit var searchView: SearchView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_event, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        vpEvent.adapter = ViewPagerAdapter(childFragmentManager)
        tabEvent.setupWithViewPager(vpEvent)

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        mContext = this.activity!!
    }

    companion object {
        fun newInstance(): EventFragment = EventFragment()
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        val mSearch = menu.findItem(R.id.idSearch)
        searchView = mSearch?.actionView as SearchView

        searchView.setOnQueryTextListener(this)
        searchView.queryHint = "Search..."

        super.onCreateOptionsMenu(menu, inflater)
    }


    internal class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = listOf(
                PastEventFragment(),
                NextFragment()
        )

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return when (position) {
                0 -> "Past"
                else -> "Next"

            }
        }


    }


}
