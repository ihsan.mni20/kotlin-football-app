package com.ihsan.footballapps.views.team

import com.ihsan.footballapps.model.League
import com.ihsan.footballapps.model.Team

interface TeamsView{
    fun showTeamList (data:List<Team>)
    fun showLeague (data:List<League>)
}