package com.ihsan.footballapps.views.search

import com.ihsan.footballapps.api.ApiClient
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class SearchPresenter(val view: SearchViews, private val mainSchedulers: Scheduler) : AnkoLogger {

    private val apiClient by lazy {
        ApiClient.create()
    }

    private var disposable: Disposable? = null

    fun requestSearchEvent(textSearch: String) {
        disposable = apiClient?.getSearchEvent(textSearch)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(mainSchedulers)
                ?.subscribe({

                    view.showEventList(it.event)
                }, {
                    view.showEventList(emptyList())
                    info(it.message.toString())

                })
    }

    fun requestSearchTeam(strTeam: String) {

        disposable = apiClient?.getSearchTeams(strTeam)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(mainSchedulers)
                ?.subscribe({
                    view.showTeamList(it.teams)

                }, {
                    view.showTeamList(emptyList())
                    info(it.message.toString())

                })
    }

}