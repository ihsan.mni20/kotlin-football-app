package com.ihsan.footballapps.views.team.detailteam

import com.ihsan.footballapps.model.Team

interface DetailTeamsView{
    fun showTeam (data:List<Team>)
}