package com.ihsan.footballapps.views.team.playerteam

import com.ihsan.footballapps.api.ApiClient
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info


class PlayersTeamsPresenter(val view: PlayersTeamsView, private val mainSchedulers: Scheduler) : AnkoLogger {

    private val apiClient by lazy {
        ApiClient.create()
    }

    private var disposable: Disposable? = null

    fun requestPlayersList(strTeam: String) {
        disposable = apiClient?.getPlayersTeams(strTeam)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(mainSchedulers)
                ?.subscribe({
                    view.showPlayerTeam(it.player)

                }, {
                    view.showPlayerTeam(emptyList())

                    info(it.message.toString())

                })
    }

}