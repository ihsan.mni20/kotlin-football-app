package com.ihsan.footballapps.views.search

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.ihsan.footballapps.R
import com.ihsan.footballapps.adapter.EventsAdapter
import com.ihsan.footballapps.adapter.TeamsAdapter
import com.ihsan.footballapps.model.Event
import com.ihsan.footballapps.model.Team
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_search.*

class SearchActivity : AppCompatActivity(), SearchViews {

    private lateinit var searchPresenter: SearchPresenter
    private lateinit var adapterEvent: EventsAdapter
    private lateinit var adapterTeam: TeamsAdapter
    private var events: MutableList<Event> = mutableListOf()
    private var teams: MutableList<Team> = mutableListOf()
    private lateinit var toolbar: ActionBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        toolbar = supportActionBar!!
        toolbar.setDisplayShowHomeEnabled(true)
        toolbar.setDisplayHomeAsUpEnabled(true)


        val textSearch = intent.getStringExtra("searchTxt")
        val isTeams = intent.getBooleanExtra("isTeam", false)

        rvListSearch.layoutManager = LinearLayoutManager(this)

        if(isTeams){

            adapterTeam = TeamsAdapter(this, teams)
            rvListSearch.adapter = adapterTeam

            searchPresenter = SearchPresenter(this, AndroidSchedulers.mainThread())
            textSearch?.let { searchPresenter.requestSearchTeam(it) }

        }else{

            adapterEvent = EventsAdapter(this, events)
            rvListSearch.adapter = adapterEvent

            searchPresenter = SearchPresenter(this, AndroidSchedulers.mainThread())
            textSearch?.let { searchPresenter.requestSearchEvent(it) }
        }
    }

    override fun showEventList(data: List<Event>) {

        events.clear()
        events.addAll(data)
        adapterEvent.notifyDataSetChanged()

        pgSearch.visibility = View.INVISIBLE
        rvListSearch.visibility = View.VISIBLE
        tvNoDataSearch.visibility = View.INVISIBLE

        if(events.isEmpty()) {
            tvNoDataSearch.visibility = View.VISIBLE
            rvListSearch.visibility = View.INVISIBLE
            events.clear()

        }
    }

    override fun showTeamList(data: List<Team>) {
        teams.clear()
        teams.addAll(data)
        adapterTeam.notifyDataSetChanged()

        pgSearch.visibility = View.INVISIBLE
        rvListSearch.visibility = View.VISIBLE
        tvNoDataSearch.visibility = View.INVISIBLE

        if (teams.isEmpty()) {
            tvNoDataSearch.visibility = View.VISIBLE
            rvListSearch.visibility = View.INVISIBLE
            teams.clear()

        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {

            android.R.id.home -> {
                finish()
                true
            }

            else -> super.onOptionsItemSelected(item)

        }
    }
}
