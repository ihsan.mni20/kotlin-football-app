package com.ihsan.footballapps.views.team.playerteam


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ihsan.footballapps.R
import com.ihsan.footballapps.adapter.PlayersAdapter
import com.ihsan.footballapps.model.Player
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_player_team.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class PlayerTeamFragment : Fragment(), PlayersTeamsView, AnkoLogger {

    private var players: MutableList<Player> = mutableListOf()
    private lateinit var adapter: PlayersAdapter
    private lateinit var playersTeamsPresenter: PlayersTeamsPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_player_team, container, false)

        val rvList = view.findViewById(R.id.rvListPlayer) as RecyclerView
        rvList.layoutManager = LinearLayoutManager(view.context)

        adapter = PlayersAdapter(view.context, players)

        rvList.adapter = adapter
        val nameTeam = arguments?.getString("strTeam")

        info(nameTeam.toString())

        playersTeamsPresenter = PlayersTeamsPresenter(this, AndroidSchedulers.mainThread())
        playersTeamsPresenter.requestPlayersList(nameTeam.toString())
        return view

    }

    override fun showPlayerTeam(data: List<Player>) {

        players.clear()
        players.addAll(data)
        adapter.notifyDataSetChanged()

        rvListPlayer.visibility = View.VISIBLE
        tvNoDataPlayer.visibility = View.INVISIBLE
        pgPlayer.visibility = View.INVISIBLE

        if (players.isEmpty()) {
            tvNoDataPlayer.visibility = View.VISIBLE
            rvListPlayer.visibility = View.INVISIBLE
            players.clear()

        }
    }


}
