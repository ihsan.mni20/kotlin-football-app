package com.ihsan.footballapps.views.event.pastevent

import com.ihsan.footballapps.model.Event
import com.ihsan.footballapps.model.League

interface PastEventView{
    fun showEventList (data:List<Event>)
    fun showLeague (data:List<League>)
}