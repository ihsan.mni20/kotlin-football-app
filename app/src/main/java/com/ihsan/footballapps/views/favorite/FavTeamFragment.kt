package com.ihsan.footballapps.views.favorite


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ihsan.footballapps.R
import com.ihsan.footballapps.adapter.TeamsAdapter
import com.ihsan.footballapps.model.Team
import com.ihsan.footballapps.utils.favDatabase
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.support.v4.ctx

class FavTeamFragment : Fragment() {

    private var teams: MutableList<Team> = mutableListOf()
    private lateinit var adapter: TeamsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_fav_team, container, false)
        val rvList = view.findViewById(R.id.rvListFavTeam) as RecyclerView
        adapter = TeamsAdapter(ctx, teams)
        rvList.adapter = adapter
        rvList.layoutManager = LinearLayoutManager(ctx)

        teams.clear()

        showFavTeamList()

        return view

    }

    private fun showFavTeamList() {
        context?.favDatabase?.use {

            val result = select(Team.TABLE_FAVORITE_TEAM)
            val favorite = result.parseList(classParser<Team>())
            teams.addAll(favorite)
            adapter.notifyDataSetChanged()

        }

    }

    override fun onResume() {
        teams.clear()
        showFavTeamList()
        super.onResume()
    }


}