package com.ihsan.footballapps.views

import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.ihsan.footballapps.R
import com.ihsan.footballapps.views.event.EventFragment
import com.ihsan.footballapps.views.favorite.FavoritesFragment
import com.ihsan.footballapps.views.team.TeamsFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var toolbar: ActionBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = supportActionBar!!
        toolbar.elevation = 0F

        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        navView.selectedItemId = R.id.navigationMatch

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigationMatch -> {
                val eventFragment = EventFragment.newInstance()
                openFragment(eventFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigationTeams -> {
                val teamsFragment = TeamsFragment.newInstance()
                openFragment(teamsFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigationFav -> {
                val favoritesFragment = FavoritesFragment.newInstance()
                openFragment(favoritesFragment)
                return@OnNavigationItemSelectedListener true
            }

        }
        true
    }


    private fun openFragment(fragment: Fragment) {

        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()

    }
}
