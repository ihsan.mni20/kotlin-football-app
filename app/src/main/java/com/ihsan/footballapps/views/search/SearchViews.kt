package com.ihsan.footballapps.views.search

import com.ihsan.footballapps.model.Event
import com.ihsan.footballapps.model.Team

interface SearchViews{
    fun showEventList (data:List<Event>)
    fun showTeamList (data:List<Team>)
}