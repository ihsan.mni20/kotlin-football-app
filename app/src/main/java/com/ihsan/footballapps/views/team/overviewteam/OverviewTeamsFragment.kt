package com.ihsan.footballapps.views.team.overviewteam


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.ihsan.footballapps.R


class OverviewTeamsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val views = inflater.inflate(R.layout.fragment_overview_teams, container, false)
        val over = views.findViewById(R.id.tvOverview) as TextView
        val strOver = arguments?.getString("ovr")

        if (strOver == null) {
            over.text = getString(R.string.no_data)
        } else {
            over.text = strOver
        }

        return views
    }

}
