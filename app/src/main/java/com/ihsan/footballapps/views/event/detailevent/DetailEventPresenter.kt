package com.ihsan.footballapps.views.event.detailevent

import com.ihsan.footballapps.api.ApiClient
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class DetailEventPresenter(val view: DetailEventView, private val mainSchedulers: Scheduler) : AnkoLogger {

    private val apiClient by lazy {
        ApiClient.create()
    }

    private var disposable: Disposable? = null

    fun requestTeam(idTeam: String?, boolean: Boolean) {

        disposable = idTeam?.let { it ->
            apiClient?.getTeam(it)
                    ?.subscribeOn(Schedulers.io())
                    ?.observeOn(mainSchedulers)
                    ?.subscribe({
                        view.showTeam(it.teams, boolean)

                    }, {
                        info(it.message.toString())

                    })
        }
    }

    fun requestEvent(idEvent: String, boolean: Boolean) {

        disposable = apiClient?.getEvent(idEvent)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(mainSchedulers)
                ?.subscribe({
                    view.showEvent(it.events, boolean)

                }, {
                    info(it.message.toString())
                })
    }
}