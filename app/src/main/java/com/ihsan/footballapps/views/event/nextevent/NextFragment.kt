package com.ihsan.footballapps.views.event.nextevent


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ihsan.footballapps.R
import com.ihsan.footballapps.adapter.EventsAdapter
import com.ihsan.footballapps.model.Event
import com.ihsan.footballapps.model.League
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_next.*
import org.jetbrains.anko.support.v4.ctx

class NextFragment : Fragment(), NextEventView{


    private var events: MutableList<Event> = mutableListOf()
    private lateinit var adapter: EventsAdapter
    private lateinit var nextEventPresenter: NextEventPresenter
    private var nameLeague: ArrayList<String> = arrayListOf()
    var idLeague: ArrayList<String> = arrayListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_next, container, false)

        val rvList = view.findViewById(R.id.rvListNext) as RecyclerView
        rvList.layoutManager = LinearLayoutManager(view.context)

        adapter = EventsAdapter(view.context, events)

        rvList.adapter = adapter


        nextEventPresenter = NextEventPresenter(this, AndroidSchedulers.mainThread())
        nextEventPresenter.requestLeague()

        return view

    }

    override fun showLeague(data: List<League>) {

        for (i in data.indices) {
            nameLeague.add(data[i].strLeague.toString())
            idLeague.add(data[i].leagueId.toString())
        }
        val spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, nameLeague)

        idSpinnerNext.adapter = spinnerAdapter
        idSpinnerNext.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, spinnerItems: Long) {

               // context?.toast(idLeague[position])
                nextEventPresenter.requestNextEvent(idLeague[position])
            }
        }
    }


    override fun showEventList(data: List<Event>) {

        events.clear()
        events.addAll(data)
        adapter.notifyDataSetChanged()

        rvListNext.visibility = View.VISIBLE
        tvNoData.visibility = View.INVISIBLE
        pgNext.visibility = View.INVISIBLE

        if (events.isEmpty()) {
            tvNoData.visibility = View.VISIBLE
            rvListNext.visibility = View.INVISIBLE
            events.clear()

        }
    }


}