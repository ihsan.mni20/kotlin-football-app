package com.ihsan.footballapps.views.event.detailevent

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.ihsan.footballapps.R
import com.ihsan.footballapps.model.Event
import com.ihsan.footballapps.model.Team
import com.ihsan.footballapps.utils.favDatabase
import com.ihsan.footballapps.utils.getDateFormat
import com.ihsan.footballapps.utils.getTimeFormat
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_detail.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class DetailEventActivity : AppCompatActivity(), DetailEventView, AnkoLogger {

    private var menuItem: Menu? = null

    private var isFavorites: Boolean = false

    private lateinit var eventId: String

    private lateinit var events: List<Event>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val pilih = intent.getBooleanExtra("isNext", true)
        val idHome = intent.getStringExtra("idHome")
        val idAway = intent.getStringExtra("idAway")

        eventId = intent.getStringExtra("idEvent").toString()

        val detailEventPresenter = DetailEventPresenter(this, AndroidSchedulers.mainThread())
        detailEventPresenter.requestEvent(eventId, pilih)
        detailEventPresenter.requestTeam(idHome, true)
        detailEventPresenter.requestTeam(idAway, false)

        favoriteState()

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.detail_menu, menu)
        menuItem = menu
        setFavorites()

        return super.onCreateOptionsMenu(menu)
    }

    private fun favoriteState() {
        favDatabase.use {
            val result = select(Event.TABLE_FAVORITE_EVENT)
                    .whereArgs("EVENT_ID={id}",
                            "id" to eventId)
            val favorite = result.parseList(classParser<Event>())

            if (favorite.isNotEmpty()) isFavorites = true

        }
    }

    private fun addFavorite() {
        try {

            favDatabase.use {
                insert(Event.TABLE_FAVORITE_EVENT,
                        Event.EVENT_ID to events[0].eventId,
                        Event.DATE_EVENT to events[0].dateEvent,
                        Event.STR_EVENT to events[0].strEvent,

                        Event.ID_HOME_TEAM to events[0].idHomeTeam,
                        Event.STR_HOME_TEAM to events[0].strHomeTeam,
                        Event.SCORE_HOME to events[0].scoreHome,
                        Event.STR_HOME_LINEUP_GOALKEEPER to events[0].strHomeLineupGoalkeeper,
                        Event.STR_HOME_LINEUP_DEFENSE to events[0].strHomeLineupDefense,
                        Event.STR_HOME_LINEUP_MIDFIELD to events[0].strHomeLineupMidfield,
                        Event.STR_HOME_LINEUP_FORWARD to events[0].strHomeLineupForward,
                        Event.STR_HOME_LINEUP_SUBSTITUTES to events[0].strHomeLineupSubstitutes,
                        Event.STR_HOME_GOAL_DETAILS to events[0].strHomeGoalDetails,

                        Event.STR_AWAY_GOAL_DETAILS to events[0].strAwayGoalDetails,
                        Event.ID_AWAY_TEAM to events[0].idAwayTeam,
                        Event.STR_AWAY_TEAM to events[0].strAwayTeam,
                        Event.SCORE_AWAY to events[0].scoreAway,
                        Event.STR_AWAY_LINEUP_GOALKEEPER to events[0].strAwayLineupGoalkeeper,
                        Event.STR_AWAY_LINEUP_DEFENSE to events[0].strAwayLineupDefense,
                        Event.STR_AWAY_LINEUP_MIDFIELD to events[0].strAwayLineupMidfield,
                        Event.STR_AWAY_LINEUP_FORWARD to events[0].strAwayLineupForward,
                        Event.STR_AWAY_LINEUP_SUBSTITUTES to events[0].strAwayLineupSubstitutes,
                        Event.STR_TIME to events[0].strTime)
            }
        } catch (e: SQLiteConstraintException) {

        }


    }

    private fun favoriteRemove() {
        favDatabase.use {
            delete(Event.TABLE_FAVORITE_EVENT, "EVENT_ID={id}",
                    "id" to eventId)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {

            android.R.id.home -> {
                finish()
                true
            }
            R.id.addToFavorites -> {
                if (isFavorites) favoriteRemove() else addFavorite()
                isFavorites = !isFavorites
                setFavorites()

                true
            }


            else -> super.onOptionsItemSelected(item)

        }
    }

    private fun setFavorites() {
        if (isFavorites) {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorites)
        } else {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_add_to_favorites)
        }

    }

    override fun showTeam(data: List<Team>, boolean: Boolean) {
        if (boolean) {
            Picasso.get().load(data[0].teamBadge).into(imgHome)
        } else {
            Picasso.get().load(data[0].teamBadge).into(imgAway)
        }
    }


    override fun showEvent(data: List<Event>, boolean: Boolean) {

        events = data

        val strDate = data[0].dateEvent
        val strTime = data[0].strTime

        val lineupHome = data[0].strHomeLineupGoalkeeper.toString() +
                "; " + data[0].strHomeLineupDefense +
                "; " + data[0].strHomeLineupMidfield +
                "; " + data[0].strHomeLineupForward

        val lineupAway = data[0].strAwayLineupGoalkeeper.toString() +
                "; " + data[0].strAwayLineupDefense +
                "; " + data[0].strAwayLineupMidfield +
                "; " + data[0].strAwayLineupForward

        clubHome.text = data[0].strHomeTeam
        clubAway.text = data[0].strAwayTeam
        scoreAway.text = data[0].scoreAway
        scoreHome.text = data[0].scoreHome
        awayGoal.text = data[0].strAwayGoalDetails.toString().replace(";", "\n")
        homeGoal.text = data[0].strHomeGoalDetails.toString().replace(";", "\n")
        homeSubstitutes.text = data[0].strHomeLineupSubstitutes.toString().replace("; ", "\n")
        awaySubstitutes.text = data[0].strAwayLineupSubstitutes.toString().replace("; ", "\n")
        homeLineup.text = lineupHome.replace("; ", "\n")
        awayLineup.text = lineupAway.replace("; ", "\n")
        dateEvent.text = getDateFormat(strTime, strDate)
        timeEvents.text = getTimeFormat(strTime, strDate)

        if (boolean) {
            awayGoal.text = "-"
            homeGoal.text = "-"
            homeSubstitutes.text = "-"
            awaySubstitutes.text = "-"
            homeLineup.text = "-"
            awayLineup.text = "-"
        }
    }

}
