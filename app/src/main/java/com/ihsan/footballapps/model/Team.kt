package com.ihsan.footballapps.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Team(

        @SerializedName("idTeam")
        var teamId: String? = null,

        @SerializedName("strTeam")
        var teamName: String? = null,

        @SerializedName("intFormedYear")
        var teamYear: String? = null,

        @SerializedName("strStadium")
        var teamStadium: String? = null,

        @SerializedName("strDescriptionEN")
        var teamDescribe: String? = null,

        @SerializedName("strTeamBadge")
        var teamBadge: String? = null


) : Parcelable{
        companion object {

                const val TABLE_FAVORITE_TEAM = "TABLE_FAVORITE_TEAM"
                const val TEAM_ID = "TEAM_ID"
                const val TEAM_NAME = "TEAM_NAME"
                const val TEAM_YEAR = "TEAM_YEAR"
                const val TEAM_STADIUM = "TEAM_STADIUM"
                const val TEAM_DESCRIPTION = "TEAM_DESCRIPTION"
                const val TEAM_BADGE = "TEAM_BADGE"
        }
}