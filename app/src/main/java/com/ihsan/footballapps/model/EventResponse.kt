package com.ihsan.footballapps.model

data class EventResponse(
        val events: List<Event>,
        val event: List<Event>,
        val teams: List<Team>,
        val player: List<Player>,
        val players: List<Player>,
        val countrys: List<League>
)