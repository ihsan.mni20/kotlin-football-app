package com.ihsan.footballapps.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Event(

        @SerializedName("idEvent")
        var eventId: String? = null,

        @SerializedName("dateEvent")
        var dateEvent: String? = null,

        @SerializedName("strEvent")
        var strEvent: String? = null,


        ////HOME TEAM
        @SerializedName("idHomeTeam")
        var idHomeTeam: String? = null,

        @SerializedName("strHomeTeam")
        var strHomeTeam: String? = null,

        @SerializedName("intHomeScore")
        var scoreHome: String? = null,

        @SerializedName("strHomeLineupGoalkeeper")
        var strHomeLineupGoalkeeper: String? = null,

        @SerializedName("strHomeLineupDefense")
        var strHomeLineupDefense: String? = null,

        @SerializedName("strHomeLineupMidfield")
        var strHomeLineupMidfield: String? = null,

        @SerializedName("strHomeLineupForward")
        var strHomeLineupForward: String? = null,

        @SerializedName("strHomeLineupSubstitutes")
        var strHomeLineupSubstitutes: String? = null,

        @SerializedName("strHomeGoalDetails")
        var strHomeGoalDetails: String? = null,

        ///AWAY TEAM

        @SerializedName("strAwayGoalDetails")
        var strAwayGoalDetails: String? = null,

        @SerializedName("idAwayTeam")
        var idAwayTeam: String? = null,

        @SerializedName("strAwayTeam")
        var strAwayTeam: String? = null,

        @SerializedName("intAwayScore")
        var scoreAway: String? = null,

        @SerializedName("strAwayLineupGoalkeeper")
        var strAwayLineupGoalkeeper: String? = null,

        @SerializedName("strAwayLineupDefense")
        var strAwayLineupDefense: String? = null,

        @SerializedName("strAwayLineupMidfield")
        var strAwayLineupMidfield: String? = null,

        @SerializedName("strAwayLineupForward")
        var strAwayLineupForward: String? = null,

        @SerializedName("strAwayLineupSubstitutes")
        var strAwayLineupSubstitutes: String? = null,

        @SerializedName("strTime")
        var strTime: String? = null


) : Parcelable {
    companion object {
        const val TABLE_FAVORITE_EVENT: String = "TABLE_FAVORITE_EVENT"
        const val EVENT_ID: String = "EVENT_ID"
        const val DATE_EVENT: String = "DATE_EVENT"
        const val STR_EVENT: String = "STR_EVENT"

        const val ID_HOME_TEAM: String = "ID_HOME_TEAM"
        const val STR_HOME_TEAM: String = "STR_HOME_TEAM"
        const val SCORE_HOME: String = "SCORE_HOME"
        const val STR_HOME_LINEUP_GOALKEEPER: String = "STR_HOME_LINEUP_GOALKEEPER"
        const val STR_HOME_LINEUP_DEFENSE: String = "STR_HOME_LINEUP_DEFENSE"
        const val STR_HOME_LINEUP_MIDFIELD: String = "STR_HOME_LINEUP_MIDFIELD"
        const val STR_HOME_LINEUP_FORWARD: String = "STR_HOME_LINEUP_FORWARD"
        const val STR_HOME_LINEUP_SUBSTITUTES: String = "STR_HOME_LINEUP_SUBSTITUTES"
        const val STR_HOME_GOAL_DETAILS: String = "STR_HOME_GOAL_DETAILS"

        const val STR_AWAY_GOAL_DETAILS: String = "STR_AWAY_GOAL_DETAILS"
        const val ID_AWAY_TEAM: String = "ID_AWAY_TEAM"
        const val STR_AWAY_TEAM: String = "STR_AWAY_TEAM"
        const val SCORE_AWAY: String = "SCORE_AWAY"
        const val STR_AWAY_LINEUP_GOALKEEPER: String = "STR_AWAY_LINEUP_GOALKEEPER"
        const val STR_AWAY_LINEUP_DEFENSE: String = "STR_AWAY_LINEUP_DEFENSE"
        const val STR_AWAY_LINEUP_MIDFIELD: String = "STR_AWAY_LINEUP_MIDFIELD"
        const val STR_AWAY_LINEUP_FORWARD: String = "STR_AWAY_LINEUP_FORWARD"
        const val STR_AWAY_LINEUP_SUBSTITUTES: String = "STR_AWAY_LINEUP_SUBSTITUTES"

        const val STR_TIME: String = "STR_TIME"
    }
}