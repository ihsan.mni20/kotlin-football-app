package com.ihsan.footballapps.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class League(

        @SerializedName("idLeague")
        var leagueId: String? = null,

        @SerializedName("strLeague")
        var strLeague: String? = null

) : Parcelable