package com.ihsan.footballapps.views.search

import com.ihsan.footballapps.api.ApiClient
import com.ihsan.footballapps.model.*
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class SearchPresenterTest {

    @Mock
    private lateinit var views: SearchViews

    @Mock
    lateinit var api: ApiClient

    private lateinit var presenter: SearchPresenter

    private val mEvent: MutableList<Event> = mutableListOf()
    private val team: MutableList<Team> = mutableListOf()
    private val player: MutableList<Player> = mutableListOf()
    private val league: MutableList<League> = mutableListOf()

    private val response = EventResponse(mEvent, mEvent, team, player, player, league)
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = SearchPresenter(this.views, Schedulers.io())
    }


    @Test
    fun testRequestSearchEvent() {
        val strEvent = "Arsenal"
        `when`(api.getEvent(strEvent)).thenReturn(Observable.just(response))

        presenter.requestSearchEvent(strEvent)

        GlobalScope.launch {
            verify(views.showEventList(mEvent))
        }
    }

    @Test
    fun testRequestSearchTeam() {
        val strTeam = "Arsenal"
        `when`(api.getTeam(strTeam)).thenReturn(Observable.just(response))

        presenter.requestSearchTeam(strTeam)

        GlobalScope.launch {
            verify(views.showTeamList(team))
        }
    }

}