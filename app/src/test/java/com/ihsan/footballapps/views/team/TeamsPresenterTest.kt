package com.ihsan.footballapps.views.team

import com.ihsan.footballapps.api.ApiClient
import com.ihsan.footballapps.model.*
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class TeamsPresenterTest {

    @Mock
    private lateinit var views: TeamsView

    @Mock
    lateinit var api: ApiClient

    private lateinit var presenter: TeamsPresenter

    private val mEvent: MutableList<Event> = mutableListOf()
    private val team: MutableList<Team> = mutableListOf()
    private val player: MutableList<Player> = mutableListOf()
    private val league: MutableList<League> = mutableListOf()

    private val response = EventResponse(mEvent, mEvent, team, player, player, league)
    @Before

    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = TeamsPresenter(this.views, Schedulers.io())
    }


    @Test
    fun testRequestLeague() {
        val strSport = "Soccer"
        Mockito.`when`(api.getLeague(strSport)).thenReturn(Observable.just(response))

        presenter.requestLeague()

        GlobalScope.launch {
            Mockito.verify(views.showLeague(league))
        }
    }

    @Test
    fun testRequestTeamList() {
        val strLeague = "English League 1"
        Mockito.`when`(api.getTeam(strLeague)).thenReturn(Observable.just(response))

        presenter.requestTeamList(strLeague)

        GlobalScope.launch {
            Mockito.verify(views.showTeamList(team))
        }
    }
}