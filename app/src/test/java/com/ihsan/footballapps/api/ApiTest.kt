package com.ihsan.footballapps.api

import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class ApiTest {

    @Mock
    lateinit var api: ApiClient

    private val idLeague = "4328"
    private val strTeam = "Arsenal"
    private val strLeague = "English Premier League"
    private val idTeam = "133604"
    private val idEvent = "441613"
    private val idPlayer = "34145937"
    private val strSport = "soccer"

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun testGetEventNext() {

        api.getEventNext(idLeague)
        Mockito.verify(api).getEventNext(idLeague)
    }

    @Test
    fun testGetEventPast() {

        api.getEventPast(idLeague)
        Mockito.verify(api).getEventPast(idLeague)
    }

    @Test
    fun testGetSearchEvent() {

        api.getSearchEvent(strTeam)
        Mockito.verify(api).getSearchEvent(strTeam)
    }

    @Test
    fun testGetLeague() {

        api.getLeague(strSport)
        Mockito.verify(api).getLeague(strSport)
    }

    @Test
    fun testGetEvent() {

        api.getEvent(idEvent)
        Mockito.verify(api).getEvent(idEvent)
    }

    @Test
    fun testGetTeam() {

        api.getTeam(idTeam)
        Mockito.verify(api).getTeam(idTeam)
    }

    @Test
    fun testGetAllTeam() {

        api.getAllTeams(strLeague)
        Mockito.verify(api).getAllTeams(strLeague)
    }

    @Test
    fun testGetSearchTeam() {

        api.getSearchTeams(strTeam)
        Mockito.verify(api).getSearchTeams(strTeam)
    }

    @Test
    fun testGetPlayersTeams() {

        api.getPlayersTeams(strTeam)
        Mockito.verify(api).getPlayersTeams(strTeam)
    }

    @Test
    fun testGetPlayersDetail() {

        api.getPlayersDetail(idPlayer)
        Mockito.verify(api).getPlayersDetail(idPlayer)
    }
}