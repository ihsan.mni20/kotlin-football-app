package com.ihsan.footballapps.views

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.pressBack
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.ihsan.footballapps.R.id.*
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.instanceOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    var activityTestRule = ActivityTestRule<MainActivity>(MainActivity::class.java)


    @Test
    fun testEvent() {


        onView(allOf(withId(vpEvent), isCompletelyDisplayed())).perform(click())

        Thread.sleep(2000)
        onView(withId(idSpinnerPast))
                .check(matches(isDisplayed()))
        Thread.sleep(2000)
        onView(withId(idSpinnerPast)).perform(click())
        Thread.sleep(2000)

        onData(allOf(`is`(instanceOf(String::class.java)), `is`("English League 1"))).perform(click())
        onView(withId(idSpinnerPast)).check(matches(withSpinnerText("English League 1")))

        Thread.sleep(2000)
        onView(withId(rvListPast))
                .check(matches(isDisplayed()))
        Thread.sleep(2000)
        onView(withId(rvListPast))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
        onView(withId(addToFavorites))
                .check(matches(isDisplayed()))
        Thread.sleep(2000)
        onView(withId(addToFavorites)).perform(click())
        Thread.sleep(2000)

        pressBack()


    }

    @Test
    fun testTeam() {

        onView(withId(navView)).check(matches(isDisplayed()))
        onView(withId(navigationTeams)).perform(click())

        onView(withId(navView)).check(matches(isDisplayed()))

        Thread.sleep(2000)
        onView(withId(idSpinnerTeam))
                .check(matches(isDisplayed()))
        onView(withId(idSpinnerTeam)).perform(click())
        onData(allOf(`is`(instanceOf(String::class.java)), `is`("English League 1"))).perform(click())
        onView(withId(idSpinnerTeam)).check(matches(withSpinnerText("English League 1")))

        Thread.sleep(2000)

        onView(withId(rvListTeam))
                .check(matches(isDisplayed()))
        Thread.sleep(2000)
        onView(withId(rvListTeam))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
        Thread.sleep(2000)
        onView(withId(addToFavorites))
                .check(matches(isDisplayed()))
        Thread.sleep(2000)
        onView(withId(addToFavorites)).perform(click())

        pressBack()

    }

    @Test
    fun testFavorite() {
        onView(withId(navView)).check(matches(isDisplayed()))
        onView(withId(navigationFav)).perform(click())

        Thread.sleep(2000)
        onView(withId(rvListFavEvent))
                .check(matches(isDisplayed()))
        Thread.sleep(2000)
        onView(withId(rvListFavEvent))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        onView(withId(addToFavorites))
                .check(matches(isDisplayed()))
        onView(withId(addToFavorites)).perform(click())
        pressBack()

    }
}